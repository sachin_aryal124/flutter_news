import 'package:get_it/get_it.dart';

import 'src/core/utils/dio_client.dart';
import 'src/features/news/data/datasources/news_article_remote_datasource.dart';
import 'src/features/news/data/repositories/news_article_repository_impl.dart';
import 'src/features/news/domain/repositories/news_article_repository.dart';
import 'src/features/news/domain/usecases/get_news_articles.dart';
import 'src/features/news/presentation/bloc/news_bloc.dart';

// refers to service locator
GetIt sl = GetIt.instance;

Future<void> setup() async {
  // blocs
  sl.registerFactory(
    () => NewsBloc(
      getNewsArticles: sl(),
    )..add(const FetchNewsArticles()),
  );
  // use cases
  sl.registerLazySingleton(() => GetNewsArticles(sl()));

  // repositories
  sl.registerLazySingleton<NewsArticleRepository>(
    () => NewsArticleRepositoryImpl(sl()),
  );

  // data sources
  sl.registerLazySingleton<NewsArticleRemoteDataSource>(
    () => NewsArticleRemoteDataSourceImpl(sl()),
  );

  // core
  sl.registerLazySingleton(() => dioClient);
}
