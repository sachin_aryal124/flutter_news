import 'package:flutter/material.dart';

import 'injection_container.dart' as di;
import 'src/features/news/presentation/pages/news_home_page.dart';

part 'app_theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.setup();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: appTheme,
      routes: {
        NewsHomePage.routeName: (context) => const NewsHomePage(),
      },
    );
  }
}
