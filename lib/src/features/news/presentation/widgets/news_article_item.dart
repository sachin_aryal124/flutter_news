import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../core/widgets/empty_image_placeholder.dart';
import '../../domain/entities/news_article.dart';

class NewsArticleItem extends StatelessWidget {
  final NewsArticle newsArticle;
  const NewsArticleItem({
    super.key,
    required this.newsArticle,
  });

  @override
  Widget build(BuildContext context) {
    const double imageWidth = 120;
    const double imageHeight = 120;

    void showSnackBar() {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Could not open url'),
        ),
      );
    }

    void openUrl() async {
      bool urlOpened = await launchUrl(Uri.parse(newsArticle.url));
      if (!urlOpened) showSnackBar();
    }

    return Card(
      child: InkWell(
        borderRadius: BorderRadius.circular(8.0),
        onTap: openUrl,
        child: Ink(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                newsArticle.urlToImage == null
                    ? const EmptyImagePlaceholder()
                    : ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.network(
                          newsArticle.urlToImage ?? '',
                          width: imageWidth,
                          height: imageHeight,
                          fit: BoxFit.cover,
                        ),
                      ),
                const SizedBox(width: 16.0),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        DateFormat.MMMEd().format(newsArticle.publishedAt),
                        style: Theme.of(context)
                            .textTheme
                            .bodySmall
                            ?.copyWith(color: Theme.of(context).primaryColor),
                      ),
                      const SizedBox(height: 8.0),
                      Text(
                        newsArticle.title,
                        style: Theme.of(context).textTheme.titleSmall,
                      ),
                      const SizedBox(height: 4.0),
                      Text(
                        newsArticle.description ?? "",
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.bodySmall,
                      ),
                      const SizedBox(height: 8.0),
                      Text(
                        newsArticle.author ?? "No author",
                        style: Theme.of(context).textTheme.titleSmall,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
