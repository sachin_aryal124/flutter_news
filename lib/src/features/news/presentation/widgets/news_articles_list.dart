import 'package:flutter/material.dart';

import '../../domain/entities/news_article.dart';
import 'news_article_item.dart';

class NewsArticleList extends StatelessWidget {
  final List<NewsArticle> newsArticles;
  const NewsArticleList({
    super.key,
    required this.newsArticles,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (context, index) => const SizedBox(height: 8.0),
      itemCount: newsArticles.length,
      itemBuilder: (context, index) {
        NewsArticle newsArticle = newsArticles[index];
        return NewsArticleItem(newsArticle: newsArticle);
      },
    );
  }
}
