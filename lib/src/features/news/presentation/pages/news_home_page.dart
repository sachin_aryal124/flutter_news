import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../injection_container.dart';
import '../../../../core/widgets/error_view.dart';
import '../../../../core/widgets/loading_view.dart';
import '../bloc/news_bloc.dart';
import '../widgets/news_articles_list.dart';

class NewsHomePage extends StatelessWidget {
  static const routeName = "/";

  const NewsHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Home"),
        ),
        body: BlocProvider(
          create: (_) => sl<NewsBloc>(),
          child: BlocBuilder<NewsBloc, NewsState>(
            builder: (context, state) {
              if (state is NewsLoading) return const LoadingView();
              if (state is NewsError) return ErrorView(message: state.message);
              if (state is NewsLoaded) {
                return NewsArticleList(newsArticles: state.newsArticles);
              }
              return const SizedBox();
            },
          ),
        ),
      ),
    );
  }
}
