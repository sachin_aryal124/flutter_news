part of 'news_bloc.dart';

abstract class NewsEvent extends Equatable {
  const NewsEvent();

  @override
  List<Object> get props => [];
}

class FetchNewsArticles extends NewsEvent {
  final String? country;

  const FetchNewsArticles({this.country});
}
