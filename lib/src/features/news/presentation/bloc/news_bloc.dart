// ignore_for_file: depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../domain/entities/news_article.dart';
import '../../domain/usecases/get_news_articles.dart';

part 'news_event.dart';
part 'news_state.dart';

class NewsBloc extends Bloc<NewsEvent, NewsState> {
  final GetNewsArticles getNewsArticles;
  NewsBloc({required this.getNewsArticles}) : super(NewsInitial()) {
    on<FetchNewsArticles>(_fetchNewsArticles);
  }

  Future<void> _fetchNewsArticles(
    FetchNewsArticles event,
    Emitter<NewsState> emit,
  ) async {
    emit(NewsLoading());
    try {
      List<NewsArticle> newsArticles = await getNewsArticles();
      emit(NewsLoaded(newsArticles: newsArticles));
    } catch (e) {
      emit(NewsError(message: e.toString()));
    }
  }
}
