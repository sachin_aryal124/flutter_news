import 'package:news_app/src/features/news/data/models/news_article_model.dart';

import '../../domain/repositories/news_article_repository.dart';
import '../datasources/news_article_remote_datasource.dart';

class NewsArticleRepositoryImpl implements NewsArticleRepository {
  final NewsArticleRemoteDataSource newsArticleRemoteDataSource;

  NewsArticleRepositoryImpl(this.newsArticleRemoteDataSource);

  @override
  Future<List<NewsArticleModel>> getNewsArticles({String? country}) {
    return newsArticleRemoteDataSource.getNewsArticles(country: country);
  }
}
