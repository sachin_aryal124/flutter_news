import 'package:json_annotation/json_annotation.dart';

import '../../domain/entities/news_article.dart';

part 'news_article_model.g.dart';

@JsonSerializable(explicitToJson: true)
class NewsArticleModel extends NewsArticle {
  NewsArticleModel({
    required super.author,
    required super.title,
    required super.description,
    required super.url,
    required super.urlToImage,
    required super.publishedAt,
    required super.content,
  });

  factory NewsArticleModel.fromJson(Map<String, dynamic> json) =>
      _$NewsArticleModelFromJson(json);

  Map<String, dynamic> toJson() => _$NewsArticleModelToJson(this);
}
