import 'package:dio/dio.dart';

import '../models/news_article_model.dart';

abstract class NewsArticleRemoteDataSource {
  Future<List<NewsArticleModel>> getNewsArticles({String? country});
}

class NewsArticleRemoteDataSourceImpl implements NewsArticleRemoteDataSource {
  final Dio dio;

  NewsArticleRemoteDataSourceImpl(this.dio);

  @override
  Future<List<NewsArticleModel>> getNewsArticles({
    String? country,
  }) async {
    String newsCountry = country ?? "us";
    Response response = await dio.get(
      '/top-headlines',
      queryParameters: {
        "country": newsCountry,
      },
    );
    List<dynamic> newsArticlesJson = response.data['articles'];
    List<NewsArticleModel> newsArticles =
        newsArticlesJson.map((e) => NewsArticleModel.fromJson(e)).toList();
    return newsArticles;
  }
}
