import '../entities/news_article.dart';

abstract class NewsArticleRepository {
  Future<List<NewsArticle>> getNewsArticles({String? country});
}
