import '../entities/news_article.dart';
import '../repositories/news_article_repository.dart';

class GetNewsArticles {
  final NewsArticleRepository newsArticleRepository;

  GetNewsArticles(this.newsArticleRepository);

  Future<List<NewsArticle>> call({
    String? country,
  }) async {
    return await newsArticleRepository.getNewsArticles(
      country: country,
    );
  }
}
