import 'package:dio/dio.dart';

import '../../env/env.dart';

BaseOptions baseOptions = BaseOptions(
  baseUrl: "https://newsapi.org/v2",
  queryParameters: {
    "apiKey": Env.newsApiKey,
  },
);

Dio dioClient = Dio(baseOptions);
