import 'package:flutter/material.dart';

class EmptyImagePlaceholder extends StatelessWidget {
  final double width;
  final double height;
  final double borderRadius;
  final Widget? child;
  const EmptyImagePlaceholder({
    super.key,
    this.width = 120,
    this.height = 120,
    this.borderRadius = 8.0,
    this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.circular(borderRadius),
      ),
      child: child ??
          Center(
            child: Text(
              'No Image',
              style: Theme.of(context).textTheme.titleSmall,
            ),
          ),
    );
  }
}
